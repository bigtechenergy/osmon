"""
Script to ping all servers in a single OpenStack project.
Currently has no error handling functionality. 
Will fail on missing environment variables or invalid OpenStack credentials.

NOTE: MUST BE RUN AS ROOT USER. 
To keep own environment variables as root user run sudo with the -E flag
e.g. sudo -E python3 ping.py
"""
import asyncio
import os
from pathlib import Path
from typing import Iterable, List, Optional

import click

from .cli import print_err, print_std, quiet_init
from .config import create_config_base, get_config, Config
from .fs import CONFIG_FILE
from .mail import EmailServer, notify_failure
from .setup import setup_new_config, startup_setup
from .tester import ServerTester
from .model.server import OpenStackServer


@click.command()
@click.option(
    "--config",
    help="Custom config path [env: OSM_CONFIG]",
    envvar="OSM_CONFIG",
    default=CONFIG_FILE,
    show_default=True,
    type=click.Path()
)
@click.option(
    "--dryrun",
    help="Test servers but don't take any actions.",
    is_flag=True,
)
@click.option(
    "--noping",
    help="Do not send ICMP ping requests. Useful if program cannot run as root user.",
    is_flag=True,
)
@click.option(
    "--newconfig",
    help="Generate a new config file at <PATH>.",
    type=click.Path(),
    default=None,
)
@click.option(
    "--quiet",
    help="Suppress stdout.",
    is_flag=True,
)
def run(
    config: str,
    dryrun: bool,
    noping: bool,
    newconfig: str,
    quiet: bool,
) -> None:

    ############################
    #           SETUP          #
    ############################

    # Flag: --quiet
    # Init quiet mode
    if quiet:
        quiet_init()

    # Run startup setup that initalizes files and directories
    startup_setup(config)

    # Flag: --newconfig
    # Create new config and exit
    if newconfig:
        setup_new_config(newconfig)
        exit(0) 

    # Load config
    cfg = get_config(config)
    
    # Flag: --noping
    if noping: # CLI flags override config values
        cfg.noping = noping
   
    # Flag: --dryrun
    if dryrun:
        cfg.dryrun = dryrun

    ############################
    #       TEST SERVERS       #
    ############################

    failed = test_and_fix(cfg)

    ############################
    #           NOTIFY         #
    ############################

    # List failed servers
    print_err("Failed:")
    print_err(failed)

    # Send notification for any failed servers
    notify_failure(failed, cfg)


def test_and_fix(config: Config) -> List[OpenStackServer]:
    # Test server connectivity
    tester = ServerTester(config)
    failed = tester.run()
    if not failed:
        print_std("All servers were pinged successfully!")
        exit(0)
    
    # Attempt to fix failed servers
    failed = tester.fix_failed(failed)
    return failed


if __name__ == "__main__":
    run()
