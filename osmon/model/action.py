from dataclasses import dataclass
from typing import Any, Callable
from ..enums import Measure


@dataclass
class Action:
    action: Callable[[Any], Any]
    measure: Measure
    description: str = ""

