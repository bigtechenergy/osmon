from dataclasses import dataclass
from typing import Optional

from httpx import Response as HTTPResponse
from pythonping.executor import Response, ResponseList

from ..enums import Status


@dataclass
class PingResults:
    ip: str
    responses: ResponseList

    @property
    def status(self) -> Status:
        status = Status.FAIL
        if all(r.success for r in self.responses):
            status = Status.OK
        elif all(not r.success for r in self.responses):
            status = Status.FAIL
        else:
            status = Status.PARTIAL_FAIL
        return status


@dataclass
class HTTPResults:
    ip: str
    port: int
    response: Optional[HTTPResponse] = None
    exception: Optional[Exception] = None

    @property
    def status(self) -> Status:
        # TODO: Add more Statuses to indicate Timeout, ConnectionError, etc.
        status = Status.FAIL # default to FAIL
        if self.response:
            status = Status.OK if not self.response.is_error else Status.FAIL
        return status
