from typing import Any

import click

def quiet_init() -> None:
    import os
    import sys
    f = open(os.devnull, 'w')
    sys.stdout = f


def print_std(msg: Any, *args, **kwargs) -> None:
    click.echo(msg, *args, **kwargs)


def print_err(msg: Any, *args, **kwargs) -> None:
    click.echo(msg, *args, err=True, **kwargs)
