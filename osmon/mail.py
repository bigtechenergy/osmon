import smtplib
import ssl
from email.message import EmailMessage
from typing import List

import click

from .cli import print_err
from .config import Config
from .model.server import OpenStackServer


class EmailServer:
    def __init__(self, url: str, username: str, password: str) -> None:
        self.username = username
        # self.password = password # obviously we shouldn't store this in plain text
        context = ssl.create_default_context()
        self.server = smtplib.SMTP_SSL(url, 465, context=context)
        self.server.login(username, password)

    def close(self) -> None:
        self.server.close()

    def send_email(self, to: List[str], content: str) -> None:
        self.server.sendmail(
            from_addr=self.username, 
            to_addrs=to, 
            msg=content
        )


def notify_failure(failed: List[OpenStackServer], config: Config) -> None:
    if not config.can_send_email:
        # NOTE: 
        # Is there a point in printing this message?
        # The user should already know after CLI arguments have been parsed
        print_err("WARNING: SMTP functionality is disabled.")
        return
    
    server = EmailServer(config.smtp_url, config.smtp_username, config.smtp_password)
    try:
        _notify_failure(failed, server, config.emails)
    finally:
        server.close()


def _notify_failure(failed: List[OpenStackServer], server: EmailServer, recipients: List[str]) -> None:
    message_body = make_email_body(failed)
    server.send_email(recipients, message_body)


def make_email_body(failed: List[OpenStackServer]) -> str:
    measures = []
    for server in failed:
        measures.append(f"{server.server.name}: {', '.join([m.name for m in server.measures])}")
    m = "\n\t".join(measures)
    body = f"""
The following servers failed: {', '.join(f.server.name for f in failed)}.
Measures taken: 
    {m}
"""
    return body
