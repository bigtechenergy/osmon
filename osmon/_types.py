from typing import Union
from pathlib import Path

PathType = Union[str, Path]