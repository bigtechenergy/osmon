from pathlib import Path
from typing import Optional

from ._types import PathType
from .cli import print_err, print_std
from .config import create_config_base
from .fs import find_config_path, get_default_config_path, get_program_dir


def setup_new_config(config_path: PathType) -> Optional[Path]:
    p = Path(config_path)
    if not p.is_absolute():
        # Save config file in program dir if path is not absolute
        program_dir = get_program_dir()
        p = program_dir / p
    
    if p.exists():
        return p

    c = create_config_base(path=p, exist_ok=True) # FIXME: CHECK IF WE ACTUALLY WANT TO OVERWRITE BY DEFAULT
    if not c:
        raise ValueError(f"oh god oh fuck {p.absolute()}") # FIXME: FIXME FIXME
    print_std(f"Generated new config file at {c.absolute()}")
    return c


def setup_program_dir() -> Path:
    """Creates program dir with an empty config in it and all required 
    directories if it doesn't exist."""
    # Check if program directory exists
    p = get_program_dir()
    if not p.exists():
        print_std(f"Creating directory at {p}")
        p.mkdir(parents=True)
    
    # TODO: setup logdir

    # # Check if config file exists
    # config_path = get_default_config_path()
    # if not config_path.exists():
    #     create_config_base(config_path)
    return p


def startup_setup(config: PathType) -> None:
    setup_program_dir()
    # setup_directories()
    setup_new_config(config)