from enum import Enum, auto


class Status(Enum):
    OK = auto()
    PARTIAL_FAIL = auto()
    FAIL = auto()

class Measure(Enum):
    """Measures taken in an attempt to fix a server."""
    NONE = auto()
    START = auto()
    REBOOT_SOFT = auto()
    REBOOT_HARD = auto()
    RESIZE_UP = auto()
    RESIZE_DOWN = auto()
