import sys
from pathlib import Path

import pytest
from osmon.fs import (LinuxDirs, MacDirs, WindowDirs, get_log_dir,
                            get_program_dir)

if sys.platform == "win32" or sys.platform == "cygwin":
    p = Path().home() / ".osmon"
    d = WindowDirs()
elif sys.platform == "linux":
    p = Path().home() / ".local" / "osmon"
    d = LinuxDirs()
elif sys.platform == "darwin":
    p = Path().home() / ".osmon"
    d = MacDirs()


def test_rootdir() -> None:
    assert Path().home() == d.root


def test_get_program_dir() -> None:
    assert get_program_dir() == p


def test_programdir() -> None:
    assert p == d.programdir


def test_get_log_dir() -> None:
    assert get_log_dir() == (p / "logs")


def test_logdir() -> None:
    assert (p / "logs") == d.logdir

