import os
from pathlib import Path
from typing import Optional

import pytest

ENV_AUTH_URL = "OSM_AUTH_URL"
ENV_APP_ID = "OSM_APPLICATION_CREDENTIAL_ID"
ENV_APP_SECRET = "OSM_APPLICATION_CREDENTIAL_SECRET"

@pytest.fixture(scope="session")
def config_file(tmpdir_factory) -> Path:
    CONF_CONTENT = """network: imt3003
auth_url: https://api.skyhigh.iik.ntnu.no:5000
application_credential_id: mycredid
application_credential_secret: mycredsecret
manager_prefix: manager
www_prefix: www
timeout: 5
emails:
  - pedeha@stud.ntnu.no
  - patricek@stud.ntnu.no
  - anderscw@stud.ntnu.no
smtp_url: smtp.gmail.com
smtp_username: myuser@gmail.com
smtp_password: mypassword
ignored:
  - www3
noping: false
dryrun: false
"""
    t = tmpdir_factory.mktemp("conf").join("config.yml")
    t.write_text(CONF_CONTENT, encoding="utf-8")
    return t


def _check_openstack_credentials_defined() -> bool:
    return bool(
        os.environ.get(ENV_AUTH_URL) and
        os.environ.get(ENV_APP_ID) and
        os.environ.get(ENV_APP_SECRET)
    )

# Fixture for skipping tests where OpenStack credentials are required
requires_openstack_credentials = pytest.mark.skipif(
    (_check_openstack_credentials_defined() == False), 
    reason=(
        "OpenStack credentials must be set via the following environment variables:"
        f"\n\t{ENV_AUTH_URL}"
        f"\n\t{ENV_APP_ID}"
        f"\n\t{ENV_APP_SECRET}"
    )
)


@pytest.fixture()
def auth_url() -> Optional[str]:
    return os.environ.get(ENV_AUTH_URL)


@pytest.fixture()
def application_credential_id() -> Optional[str]:
    return os.environ.get(ENV_APP_ID)



@pytest.fixture()
def application_credential_secret() -> Optional[str]:
    return os.environ.get(ENV_APP_SECRET)
