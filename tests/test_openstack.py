import os
import pytest
from osmon.openstack import get_openstack_client

from .conftest import requires_openstack_credentials


@requires_openstack_credentials
def test_get_openstack_client(auth_url: str, application_credential_id: str, application_credential_secret: str) -> None:
    assert get_openstack_client(auth_url, application_credential_id, application_credential_secret)

# TODO: add tests checking client values...
